\begin{thebibliography}{}

\bibitem[Aird et~al., 2011]{aird2011analyzing}
Aird, D., Ross, M.~G., Chen, W.-S., Danielsson, M., Fennell, T., Russ, C.,
  Jaffe, D.~B., Nusbaum, C., and Gnirke, A., 2011.
\newblock Analyzing and minimizing PCR amplification bias in Illumina
  sequencing libraries.
\newblock {\em Genome biology}, \textbf{12}(2):R18.

\bibitem[Barton et~al., 2017]{barton2017infinitesimal}
Barton, N.~H., Etheridge, A.~M., and V{\'e}ber, A., 2017.
\newblock The infinitesimal model: {Definition}, derivation, and implications.
\newblock {\em Theoretical population biology}, \textbf{118}:50--73.

\bibitem[Berg and Coop, 2014]{berg2014population}
Berg, J.~J. and Coop, G., 2014.
\newblock A population genetic signal of polygenic adaptation.
\newblock {\em PLoS genetics}, \textbf{10}(8).

\bibitem[Boyle et~al., 2017]{boyle2017expanded}
Boyle, E.~A., Li, Y.~I., and Pritchard, J.~K., 2017.
\newblock An expanded view of complex traits: from polygenic to omnigenic.
\newblock {\em Cell}, \textbf{169}(7):1177--1186.

\bibitem[Cai et~al., 2015a]{cai2015sparse}
Cai, N., Bigdeli, T.~B., Kretzschmar, W., Li, Y., Liang, J., Song, L., Hu, J.,
  Li, Q., Jin, W., Hu, Z., \emph{et~al.}, 2015a.
\newblock Sparse whole-genome sequencing identifies two loci for major
  depressive disorder.
\newblock {\em Nature}, \textbf{523}(7562):588--591.

\bibitem[Cai et~al., 2015b]{cai2015genetic}
Cai, N., Li, Y., Chang, S., Liang, J., Lin, C., Zhang, X., Liang, L., Hu, J.,
  Chan, W., Kendler, K.~S., \emph{et~al.}, 2015b.
\newblock Genetic control over mtDNA and its relationship to major depressive
  disorder.
\newblock {\em Current Biology}, \textbf{25}(24):3170--3177.

\bibitem[Chapman et~al., 2003]{chapman2003detecting}
Chapman, J.~M., Cooper, J.~D., Todd, J.~A., and Clayton, D.~G., 2003.
\newblock Detecting disease associations due to linkage disequilibrium using
  haplotype tags: a class of tests and the determinants of statistical power.
\newblock {\em Human heredity}, \textbf{56}(1-3):18--31.

\bibitem[Consortium et~al., 2015]{10002015global}
Consortium, . G.~P. et~al., 2015.
\newblock A global reference for human genetic variation.
\newblock {\em Nature}, \textbf{526}(7571):68--74.

\bibitem[Consortium et~al., 2007]{wellcome2007genome}
Consortium, W. T. C.~C. et~al., 2007.
\newblock Genome-wide association study of 14,000 cases of seven common
  diseases and 3,000 shared controls.
\newblock {\em Nature}, \textbf{447}(7145):661.

\bibitem[Das et~al., 2015]{das2015minimac}
Das, S., Abecasis, G., and Fuchsberger, C., 2015.
\newblock Minimac4: A next generation imputation tool for mega reference
  panels.
\newblock Abstract 1278W. Presented at the the 65th Annual Meeting of the
  American Society of Human Genetics, October 7, 2015, Baltimore, MD.

\bibitem[Dong et~al., 2016]{dong2016low}
Dong, Z., Zhang, J., Hu, P., Chen, H., Xu, J., Tian, Q., Meng, L., Ye, Y.,
  Wang, J., Zhang, M., \emph{et~al.}, 2016.
\newblock Low-pass whole-genome sequencing in clinical cytogenetics: a
  validated approach.
\newblock {\em Genetics in Medicine}, \textbf{18}(9):940--948.

\bibitem[Fisher, 1919]{fisher1919xv}
Fisher, R.~A., 1919.
\newblock {XV.—The} correlation between relatives on the supposition of
  {Mendelian} inheritance.
\newblock {\em Earth and Environmental Science Transactions of the Royal
  Society of Edinburgh}, \textbf{52}(2):399--433.

\bibitem[Gilly et~al., 2019]{gilly2019very}
Gilly, A., Southam, L., Suveges, D., Kuchenbaecker, K., Moore, R., Melloni,
  G.~E., Hatzikotoulas, K., Farmaki, A.-E., Ritchie, G., Schwartzentruber, J.,
  \emph{et~al.}, 2019.
\newblock Very low-depth whole-genome sequencing in complex trait association
  studies.
\newblock {\em Bioinformatics}, \textbf{35}(15):2555--2561.

\bibitem[Guo et~al., 2018]{guo2018global}
Guo, J., Wu, Y., Zhu, Z., Zheng, Z., Trzaskowski, M., Zeng, J., Robinson,
  M.~R., Visscher, P.~M., and Yang, J., 2018.
\newblock Global genetic differentiation of complex traits shaped by natural
  selection in humans.
\newblock {\em Nature communications}, \textbf{9}(1):1--9.

\bibitem[Harrell, 2017]{harrell2017regression}
Harrell, F., 2017.
\newblock Regression modeling strategies.
\newblock {\em BIOS}, \textbf{330}:2018.

\bibitem[Homburger et~al., 2019]{homburger2019low}
Homburger, J.~R., Neben, C.~L., Mishne, G., Zhou, A.~Y., Kathiresan, S., and
  Khera, A.~V., 2019.
\newblock Low coverage whole genome sequencing enables accurate assessment of
  common variants and calculation of genome-wide polygenic scores.
\newblock {\em Genome medicine}, \textbf{11}(1):1--12.

\bibitem[Inc., 2020a]{IlluminaGSA}
Inc., I., 2020a.
\newblock Infinium Global Screening Array-24 Kit.

\bibitem[Inc., 2020b]{IlluminaS4FlowCell}
Inc., I., 2020b.
\newblock {N}ova{S}eq Reagent Kits.

\bibitem[Inouye et~al., 2018]{inouye2018genomic}
Inouye, M., Abraham, G., Nelson, C.~P., Wood, A.~M., Sweeting, M.~J.,
  Dudbridge, F., Lai, F.~Y., Kaptoge, S., Brozynska, M., Wang, T.,
  \emph{et~al.}, 2018.
\newblock Genomic risk prediction of coronary artery disease in 480,000 adults:
  implications for primary prevention.
\newblock {\em Journal of the American College of Cardiology},
  \textbf{72}(16):1883--1893.

\bibitem[Jensen et~al., 2020]{jensen2020sorghum}
Jensen, S.~E., Charles, J.~R., Muleta, K., Bradbury, P.~J., Casstevens, T.,
  Deshpande, S.~P., Gore, M.~A., Gupta, R., Ilut, D.~C., Johnson, L.,
  \emph{et~al.}, 2020.
\newblock A sorghum practical haplotype graph facilitates genome-wide
  imputation and cost-effective genomic prediction.
\newblock {\em The Plant Genome}, \textbf{13}(1):e20009.

\bibitem[Jones et~al., 2015]{jones2015library}
Jones, M.~B., Highlander, S.~K., Anderson, E.~L., Li, W., Dayrit, M., Klitgord,
  N., Fabani, M.~M., Seguritan, V., Green, J., Pride, D.~T., \emph{et~al.},
  2015.
\newblock Library preparation methodology can influence genomic and functional
  predictions in human microbiome research.
\newblock {\em Proceedings of the National Academy of Sciences},
  \textbf{112}(45):14024--14029.

\bibitem[Judson, 1979]{judson1979eighth}
Judson, H.~F., 1979.
\newblock The eighth day of creation.
\newblock {\em New York}, \textbf{}:550.

\bibitem[Krause et~al., 2010]{krause2010complete}
Krause, J., Briggs, A.~W., Kircher, M., Maricic, T., Zwyns, N., Derevianko, A.,
  and P{\"a}{\"a}bo, S., 2010.
\newblock A complete mtDNA genome of an early modern human from Kostenki,
  Russia.
\newblock {\em Current Biology}, \textbf{20}(3):231--236.

\bibitem[Lachance and Tishkoff, 2013]{lachance2013snp}
Lachance, J. and Tishkoff, S.~A., 2013.
\newblock {SNP} ascertainment bias in population genetic analyses: why it is
  important, and how to correct it.
\newblock {\em Bioessays}, \textbf{35}(9):780--786.

\bibitem[Li et~al., 2009]{li2009genotype}
Li, Y., Willer, C., Sanna, S., and Abecasis, G., 2009.
\newblock Genotype imputation.
\newblock {\em Annual review of genomics and human genetics},
  \textbf{10}:387--406.

\bibitem[Liu et~al., 2018]{liu2018genomic}
Liu, S., Huang, S., Chen, F., Zhao, L., Yuan, Y., Francis, S.~S., Fang, L., Li,
  Z., Lin, L., Liu, R., \emph{et~al.}, 2018.
\newblock Genomic analyses from non-invasive prenatal testing reveal genetic
  associations, patterns of viral infections, and Chinese population history.
\newblock {\em Cell}, \textbf{175}(2):347--359.

\bibitem[Loh et~al., 2016]{loh2016reference}
Loh, P.-R., Danecek, P., Palamara, P.~F., Fuchsberger, C., Reshef, Y.~A.,
  Finucane, H.~K., Schoenherr, S., Forer, L., McCarthy, S., Abecasis, G.~R.,
  \emph{et~al.}, 2016.
\newblock Reference-based phasing using the {Haplotype} {Reference}
  {Consortium} panel.
\newblock {\em Nature genetics}, \textbf{48}(11):1443.

\bibitem[Marchini, 2019]{balding2019}
Marchini, J., 2019.
\newblock {\em Haplotype {Estimation} and {Genotype} {Imputation}}, chapter~3,
  pages 87--114.
\newblock John Wiley \& Sons, Ltd.

\bibitem[Marchini and Howie, 2010]{marchini2010genotype}
Marchini, J. and Howie, B., 2010.
\newblock Genotype imputation for genome-wide association studies.
\newblock {\em Nature Reviews Genetics}, \textbf{11}(7):499--511.

\bibitem[Mavaddat et~al., 2019]{mavaddat2019polygenic}
Mavaddat, N., Michailidou, K., Dennis, J., Lush, M., Fachal, L., Lee, A.,
  Tyrer, J.~P., Chen, T.-H., Wang, Q., Bolla, M.~K., \emph{et~al.}, 2019.
\newblock Polygenic risk scores for prediction of breast cancer and breast
  cancer subtypes.
\newblock {\em The American Journal of Human Genetics}, \textbf{104}(1):21--34.

\bibitem[McCarthy et~al., 2016]{mccarthy2016reference}
McCarthy, S., Das, S., Kretzschmar, W., Delaneau, O., Wood, A.~R., Teumer, A.,
  Kang, H.~M., Fuchsberger, C., Danecek, P., Sharp, K., \emph{et~al.}, 2016.
\newblock A reference panel of 64,976 haplotypes for genotype imputation.
\newblock {\em Nature genetics}, \textbf{48}(10):1279--1283.

\bibitem[Nielsen, 2004]{nielsen2004population}
Nielsen, R., 2004.
\newblock Population genetic analysis of ascertained {SNP} data.
\newblock {\em Human genomics}, \textbf{1}(3):218.

\bibitem[Pasaniuc et~al., 2012]{pasaniuc2012extremely}
Pasaniuc, B., Rohland, N., McLaren, P.~J., Garimella, K., Zaitlen, N., Li, H.,
  Gupta, N., Neale, B.~M., Daly, M.~J., Sklar, P., \emph{et~al.}, 2012.
\newblock Extremely low-coverage sequencing and imputation increases power for
  genome-wide association studies.
\newblock {\em Nature genetics}, \textbf{44}(6):631.

\bibitem[Patterson et~al., 2019]{patterson2019impact}
Patterson, J., Carpenter, E.~J., Zhu, Z., An, D., Liang, X., Geng, C., Drmanac,
  R., and Wong, G. K.-S., 2019.
\newblock Impact of sequencing depth and technology on de novo {RNA}-seq
  assembly.
\newblock {\em BMC genomics}, \textbf{20}(1):604.

\bibitem[Pritchard and Przeworski, 2001]{pritchard2001linkage}
Pritchard, J.~K. and Przeworski, M., 2001.
\newblock Linkage disequilibrium in humans: models and data.
\newblock {\em The American Journal of Human Genetics}, \textbf{69}(1):1--14.

\bibitem[Risch et~al., 1999]{risch1999genomic}
Risch, N., Spiker, D., Lotspeich, L., Nouri, N., Hinds, D., Hallmayer, J.,
  Kalaydjieva, L., McCague, P., Dimiceli, S., Pitts, T., \emph{et~al.}, 1999.
\newblock A genomic screen of autism: evidence for a multilocus etiology.
\newblock {\em The American Journal of Human Genetics},
  \textbf{65}(2):493--507.

\bibitem[Rubinacci et~al., 2020]{rubinacci2020efficient}
Rubinacci, S., Ribeiro, D., Hofmeister, R., and Delaneau, O., 2020.
\newblock Efficient phasing and imputation of low-coverage sequencing data
  using large reference panels.
\newblock {\em bioRxiv}, \textbf{}.

\bibitem[Schurz et~al., 2019]{schurz2019evaluating}
Schurz, H., M{\"u}ller, S.~J., Van~Helden, P.~D., Tromp, G., Hoal, E.~G.,
  Kinnear, C.~J., and M{\"o}ller, M., 2019.
\newblock Evaluating the accuracy of imputation methods in a five-way admixed
  population.
\newblock {\em Frontiers in genetics}, \textbf{10}:34.

\bibitem[Sella and Barton, 2019]{sella2019thinking}
Sella, G. and Barton, N.~H., 2019.
\newblock Thinking about the evolution of complex traits in the era of
  genome-wide association studies.
\newblock {\em Annual review of genomics and human genetics},
  \textbf{20}:461--493.

\bibitem[Stevenson et~al., 2019]{stevenson2019neuropsychiatric}
Stevenson, A., Akena, D., Stroud, R.~E., Atwoli, L., Campbell, M.~M., Chibnik,
  L.~B., Kwobah, E., Kariuki, S.~M., Martin, A.~R., de~Menil, V.,
  \emph{et~al.}, 2019.
\newblock Neuropsychiatric {Genetics} of {African} {Populations-Psychosis}
  ({NeuroGAP-Psychosis}): a case-control study protocol and {GWAS in Ethiopia,
  Kenya, South Africa and Uganda}.
\newblock {\em BMJ open}, \textbf{9}(2):bmjopen--2018.

\bibitem[Taliun et~al., 2019]{taliun2019sequencing}
Taliun, D., Harris, D.~N., Kessler, M.~D., Carlson, J., Szpiech, Z.~A., Torres,
  R., Taliun, S. A.~G., Corvelo, A., Gogarten, S.~M., Kang, H.~M.,
  \emph{et~al.}, 2019.
\newblock Sequencing of 53,831 diverse genomes from the {NHLBI TOPMed Program}.
\newblock {\em BioRxiv}, \textbf{}.

\bibitem[Tran et~al., 2020]{tran2020genetic}
Tran, N.~H., Vo, T.~B., Tran, N.-T., Trinh, T.-H.~N., Pham, H.-A.~T., Dao, T.
  H.~T., Nguyen, N.~M., Van, Y.-L.~T., Tran, V.~U., Vu, H.~G., \emph{et~al.},
  2020.
\newblock Genetic profiling of Vietnamese population from large-scale genomic
  analysis of non-invasive prenatal testing data.
\newblock {\em Scientific reports}, \textbf{10}(1):1--8.

\bibitem[Visscher et~al., 2012]{visscher2012five}
Visscher, P.~M., Brown, M.~A., McCarthy, M.~I., and Yang, J., 2012.
\newblock Five years of {GWAS} discovery.
\newblock {\em The American Journal of Human Genetics}, \textbf{90}(1):7--24.

\bibitem[Walsh and Lynch, 2018]{walsh2018evolution}
Walsh, B. and Lynch, M., 2018.
\newblock {\em Evolution and selection of quantitative traits}.
\newblock Oxford University Press.

\bibitem[Wasik et~al., 2019]{wasik2019comparing}
Wasik, K., Berisa, T., Pickrell, J.~K., Li, J.~H., Fraser, D.~J., King, K., and
  Cox, C., 2019.
\newblock Comparing low-pass sequencing and genotyping for trait mapping in
  pharmacogenetics.
\newblock {\em bioRxiv}, \textbf{}.

\bibitem[Wetterstrand, 2019]{wetterstrand2019dna}
Wetterstrand, K.~A., 2019.
\newblock {DNA sequencing costs: Data from the NHGRI Genome Sequencing Program
  (GSP). National Human Genome Research Institute 2019}.
\newblock \textbf{}.

\bibitem[Wood et~al., 2019]{wood2019improved}
Wood, D.~E., Lu, J., and Langmead, B., 2019.
\newblock Improved metagenomic analysis with Kraken 2.
\newblock {\em Genome biology}, \textbf{20}(1):257.

\bibitem[Xia et~al., 2019]{xia2019advanced}
Xia, Z., Jiang, Y., Drmanac, R., Shen, H., Liu, P., Li, Z., Chen, F., Jiang,
  H., Shi, S., Xi, Y., \emph{et~al.}, 2019.
\newblock Advanced Whole Genome Sequencing Using a Complete PCR-free Massively
  Parallel Sequencing (MPS) Workflow.
\newblock {\em bioRxiv}, \textbf{}.

\bibitem[Yang et~al., 2010]{yang2010common}
Yang, J., Benyamin, B., McEvoy, B.~P., Gordon, S., Henders, A.~K., Nyholt,
  D.~R., Madden, P.~A., Heath, A.~C., Martin, N.~G., Montgomery, G.~W.,
  \emph{et~al.}, 2010.
\newblock Common {SNPs} explain a large proportion of the heritability for
  human height.
\newblock {\em Nature genetics}, \textbf{42}(7):565.

\bibitem[Zhou et~al., 2018]{Zhou735}
Zhou, B., Ho, S.~S., Zhang, X., Pattni, R., Haraksingh, R.~R., and Urban,
  A.~E., 2018.
\newblock Whole-genome sequencing analysis of CNV using low-coverage and
  paired-end strategies is efficient and outperforms array-based CNV analysis.
\newblock {\em Journal of Medical Genetics}, \textbf{55}(11):735--743.

\end{thebibliography}
