Gencove SBIR VCFs

The files in this AWS s3 bucket are those used in Li et al. 2020 "Low pass sequencing increases the power of GWAS and decreases measurement error of polygenic risk scores compared to genotyping arrays".

These data can also be accessed via the following URL: `https://gencove-sbir.s3.amazonaws.com/index.html` in which case one ought to simply replace `s3://gencove-sbir/` with this URL in the paths enumerated below.  

This bucket contains three datasets of genotype calls in gzipped VCF form along with a csi/tbi index:

- files following the s3://gencove-sbir/gsa/unimputed/ prefix are the raw genotype calls for the 358 samples passing QC, as delivered from the Broad Institute. The data are on the hg19 build of the reference genome and the paths follow the pattern: `s3://gencove-sbir/gsa/unimputed/[cell_line]_[replicate_number]/[barcode]/v[12]/[barcode].vcf.gz`. Each `[barcode].vcf.gz` is the VCF file corresponding with the sample `[cell_line]_[replicate_number]`.
- files following the s3://gencove-sbir/gsa/imputed-unphased/ prefix are the GSA data imputed to the 1KGP3 haplotype reference panel for the 358 samples passing QC using minimac4. The phase information added by minimac4 is removed for these files in order to facilitate easier comparison of genotypes using tools like bcftools. The data are on the hg19 build of the reference genome and the paths follow the pattern: `s3://gencove-sbir/gsa/imputed-unphased/[cell_line]_[replicate_number].vcf.gz`
- files following the s3://gencove-sbir/sequence/ prefix are the VCFs resulting from low-pass sequencing plus imputation using `loimpute` to the 1KGP3 haplotype reference panel. The data are split up by Experiment as described in the paper. The data are on the hg19 build of the human reference genome.  

For the sequence VCFs, the sample names (and filenames) follow the following pattern:

`s3://gencove-sbir/sequence/experiment-[ABCD]/[cell_line]-[target_coverage]-[replicate_number]-[resequenced?]-[project_part].vcf.gz`

where 

- cell_line: the cell line from which the sample was taken
- target_coverage: 0 if target coverage was 0.5x, 1 if target coverage was 1.0x
- replicate_number: replicate number unique up to cell line, target coverage, and project part
- resequenced?: a designation used internally to Gencove. Not relevant for downstream analysis.
- project_par: 0 indicates that this sample was part of either experiment A or B. 1 indicates that this sample was part of experiment C. 2 indicates that this sample was part of experiment D.

So, for example, a sample name `NA20805-1-2-1-0` would indicate the this sample is of cell line NA20805 sequenced at a target coverage of 1x, corresponding with either experiment A or B.
Since experiment A was sequenced to a target coverage of 0.5x it must be part of experiment B. 
All these files are sorted into labeled experiment subdirectories anyway but this naming scheme provides a way to uniquely identify any orphaned sample. 

The raw sequence reads for experiments A-D for samples passing QC are also located in this bucket under the prefix `s3://gencove-sbir/fastq/` and are named similarly. 

The CSV in `s3://gencove-sbir/metadata/representative-cohorts.csv` is a list of the samples considered as part of each experiment's representative cohort, as described in the manuscript. 